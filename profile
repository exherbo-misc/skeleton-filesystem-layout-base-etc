# /etc/profile: login shell setup
#
# That this file is used by any Bourne-shell derivative to setup the
# environment for login shells.
#

# Load environment settings from profile.env, which is created by
# env-update from the files in /etc/env.d
if [ -e /etc/profile.env ] ; then
    . /etc/profile.env
fi

# 077 would be more secure, but 022 is generally quite realistic
umask 022

# Extract the value of EDITOR
[ -z "$EDITOR" ] && EDITOR="/usr/host/bin/vim"
export EDITOR

if [ -n "${BASH_VERSION}" ] ; then
    # Newer bash ebuilds include /etc/bash/bashrc which will setup PS1
    # including color.  We leave out color here because not all
    # terminals support it.
    if [ -f /etc/bash/bashrc ] ; then
        # Bash login shells run only /etc/profile
        # Bash non-login shells run only /etc/bash/bashrc
        # Since we want to run /etc/bash/bashrc regardless, we source it
        # from here.  It is unfortunate that there is no way to do
        # this *after* the user's .bash_profile runs (without putting
        # it in the user's dot-files), but it shouldn't make any
        # difference.
        . /etc/bash/bashrc
    else
        PS1='\u@\h \w \$ '
    fi
else
    # Setup a bland default prompt.  Since this prompt should be useable
    # on color and non-color terminals, as well as shells that don't
    # understand sequences such as \h, don't put anything special in it.
    PS1="`whoami`@`uname -n | cut -f1 -d.` \$ "
fi

for sh in /etc/profile.d/*.sh ; do
    if [ -r "$sh" ] ; then
        . "$sh"
    fi
done
unset sh
